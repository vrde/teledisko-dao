module gitlab.com/teledisko/dao/oracle

go 1.15

require (
	github.com/ethereum/go-ethereum v1.9.25
	github.com/joho/godotenv v1.3.0
	github.com/ybbus/jsonrpc/v2 v2.1.6
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
)
